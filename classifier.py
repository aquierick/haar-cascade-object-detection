import numpy as np
import cv2
import sys
import os

cascade = sys.argv[2]
stages = cascade[23:25]
window = cascade[32:34]


scalings = [0.25, 0.4, 0.5]

for scaling_factor in scalings:

    scalingStr = str(scaling_factor)[2:]
    dir = sys.argv[1]


    print("Stages: {}".format(stages))

    for filename, i in zip(os.listdir(dir), range(0, len(os.listdir(dir)))):
        name = filename[:3] + filename[4:len(filename)-4]
        print("{}|".format(i) * 20)
        print("Working in out/detected_{}_{}_{}_{}.png".format(name, stages, scalingStr, window))
        phoca_cascade = cv2.CascadeClassifier(cascade)


        img = cv2.imread(dir + filename)
        img = cv2.resize(img,None,fx=scaling_factor, fy=scaling_factor)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        phocas = phoca_cascade.detectMultiScale(gray, 1.3, 5)
        for (x,y,w,h) in phocas:
            img = cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
            roi_gray = gray[y:y+h, x:x+w]
            roi_color = img[y:y+h, x:x+w]

        img = cv2.resize(img,None,fx=1, fy=1)
        # cv2.imshow("out", img)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        name = filename[:8]
        print("Writing in out/detected_{}_{}_{}_{}.png".format(name, stages, scalingStr, window))
        print("="*40)
        cv2.imwrite("out/detected_{}_{}_{}_{}.png".format(name, stages, scalingStr, window), img)
