import os
import cv2 as cv

directory = 'neg/'

for filename in os.listdir(directory):
    print("Converting {}{}...".format(directory, filename))
    img = cv.imread(directory + filename, 0)
    cv.imwrite(directory + filename, img)
