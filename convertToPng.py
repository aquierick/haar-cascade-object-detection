import os
import cv2 as cv

directory = 'neg/'

def convert():
    for filename in os.listdir(directory):
        print("Converting {}{}...".format(directory, filename))
        img = cv.imread(directory + filename)
        newName = removeExtension(filename, 4)
        cv.imwrite(directory + newName + ".png", img)

def removeExtension(filename, leng):
    filename = filename[0:len(filename) - leng]
    return filename

convert()
